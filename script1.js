const email = document.getElementById("email");
const error=document.getElementById("error");
const form=document.getElementById("signup_form");
const sign_password=document.getElementById("sign-password");
const confirm_password=document.getElementById("confirm-password");


form.addEventListener("submit", (e) => {
  let messages = [];
  let pattern = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/;
  if (!email.value.match(pattern)) {
    messages.push("Please enter correct mail id");
  } else {
    console.log(sign_password.value);
     if(sign_password.value.length<8){
       messages.push("Password too short");
     }
     if(sign_password.value!==confirm_password.value){
         messages.push("Password not matches");
     }
  }

  if (messages.length > 0) {
    e.preventDefault();
    error.innerText = messages.join(",");
  }
});
