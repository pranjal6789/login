const email = document.getElementById("name");
const password = document.getElementById("password");
const form = document.getElementById("login_form");
const error = document.getElementById("error");
const user_error=document.getElementById("user_error");
const pass_error=document.getElementById("pass_error");

form.addEventListener("submit", (e) => {
  let messages = [];
  let pattern = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/;
  if (!email.value.match(pattern)) {
    e.preventDefault();
    user_error.innerText="Please enter valid mail id";
    
  } else {
    if (password.value.length < 8) {
      e.preventDefault();
     pass_error.innerText="Password too short"
    }
  }

  // if (messages.length > 0) {
  //   e.preventDefault();
  //   error.innerText = messages.join(",");
  // }
});
